package test.spark;

import akka.japi.JavaPartialFunction;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.CassandraStreamingJavaUtil;
import com.datastax.spark.connector.types.TypeConverter;
import com.datastax.spark.connector.types.TypeConverter$;
import kafka.serializer.StringDecoder;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.kafka.KafkaUtils;
import scala.PartialFunction;
import scala.Serializable;
import scala.Tuple2;
import scala.reflect.api.TypeTags;

import java.io.IOException;
import java.lang.Long;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.*;

/**
 * @author Valery Skavysh.
 */
public class KafkaCasandraIntegration
{
    private final static Properties properties;

    private final static String MESSAGE_KEYSPACE = "test";

    private final static String MESSAGE_TABLE = "test";

    static {
        properties = new Properties();

        try
        {
            properties.load(
                    KafkaCasandraIntegration.class.getResourceAsStream("config.properties"));
        }
        catch (IOException e)
        {
            throw new RuntimeException("Couldn't init properties!", e);
        }
    }

    public static void main(String[] args)
    {
        JavaStreamingContext jsc = init();

        String broker = properties.getProperty("spark.streaming.broker");
        String topic = properties.getProperty("spark.streaming.topic");

        Set<String> topicsSet = new HashSet<>(Collections.singletonList(topic));
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put("metadata.broker.list", broker);

        JavaPairInputDStream<String, String> stream = KafkaUtils.createDirectStream(
                jsc, String.class, String.class, StringDecoder.class, StringDecoder.class,
                kafkaParams, topicsSet);

        CassandraStreamingJavaUtil.javaFunctions(
                stream.map(tp ->
                        new Tuple2<>(
                                UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE,
                                BytesContainer.fromString(tp._2()))
                    ))
                .writerBuilder(MESSAGE_KEYSPACE, MESSAGE_TABLE,
                        CassandraJavaUtil.mapTupleToRow(Long.class, BytesContainer.class))
                .withColumnSelector(CassandraJavaUtil.someColumns("id", "data"))
                .saveToCassandra();

        jsc.start();
        jsc.awaitTermination();
    }

    private static JavaStreamingContext init()
    {
        SparkConf sparkConf = new SparkConf()
                .setAppName("KafkaCasandraIntegration")
                .set("spark.cassandra.connection.host", properties.getProperty("spark.cassandra.connection.host"));

        CassandraConnector connector = CassandraConnector.apply(sparkConf);
        try (Session session = connector.openSession())
        {
            session.execute(
                    "DROP KEYSPACE IF EXISTS " + MESSAGE_KEYSPACE);
            session.execute(String.format(
                    "CREATE KEYSPACE %s WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}",
                    MESSAGE_KEYSPACE));
            session.execute(String.format(
                    "CREATE TABLE %s.%s (id bigint PRIMARY KEY, data blob)",
                    MESSAGE_KEYSPACE, MESSAGE_TABLE) );
        }

        TypeConverter$.MODULE$.registerConverter(new TypeConverter<ByteBuffer>()
        {
            @Override
            public TypeTags.TypeTag<ByteBuffer> targetTypeTag()
            {
                return CassandraJavaUtil.typeTag(ByteBuffer.class);
            }

            @Override
            public String targetTypeName()
            {
                return targetTypeTag().tpe().toString();
            }

            @Override
            public PartialFunction<Object, ByteBuffer> convertPF()
            {
                return new JavaPartialFunction<Object, ByteBuffer>() {
                    @Override
                    public ByteBuffer apply(
                            Object o,
                            boolean isCheck)
                    {
                        if (o instanceof BytesContainer) {
                            if (isCheck) return null;
                            return convert(o);
                        } else {
                            throw noMatch();
                        }
                    }
                };
            }

            @Override
            public ByteBuffer convert(
                    Object o)
            {
                return ByteBuffer.wrap(((BytesContainer) o).getBytes());
            }
        });

        return  new JavaStreamingContext(sparkConf, Durations.seconds(2));
    }

    static class BytesContainer
    implements Serializable {
        private static final CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
        private byte[] bytes;

        BytesContainer(
                ByteBuffer buffer)
        {
            this.bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
        }

        static BytesContainer fromString(
                String string)
        throws CharacterCodingException
        {
            return new BytesContainer(
                    encoder.encode(CharBuffer.wrap(string)));
        }

        public byte[] getBytes()
        {
            return bytes;
        }
    }
}
